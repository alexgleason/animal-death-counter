Animal Death Counter
====================

A live counter showing farmed animals killed today in the United States.

![Screenshot](http://i.imgur.com/jyRpLp8.png)

License
=======
animal-death-counter is licensed under GNU GPL version 3.0. For the full license see the LICENSE file.
